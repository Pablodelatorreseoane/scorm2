package com.example.scorm2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.example.scorm2.R


class Fragment1 : Fragment(), View.OnClickListener {

    lateinit var clickbtn: EditText
    lateinit var text1: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickbtn=view.findViewById(R.id.clickbtn)
        clickbtn.setOnClickListener(this)
        text1=view.findViewById(R.id.text1)
    }

    override fun onClick(v: View?) {
        text1.text=""
    }



}